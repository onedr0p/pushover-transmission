#!/bin/bash

URL="https://api.pushover.net/1/messages.json"
API_KEY=""
USER_KEY=""

SERENITY_MOVIE_PATH="/wash/.unprocessed"
SERENITY_SERIES_PATH="/river/.unprocessed"
SERENITY_MUSIC_PATH="/river/.unprocessed"
SERENITY_SOFTWARE_PATH="/wash/Software"
TR_DOWNLOADED_PATH="${TR_TORRENT_DIR}/${TR_TORRENT_NAME}"
TITLE="Torrent downloaded"

# Copy the torrents to the XFS raids
case "${TR_DOWNLOADED_PATH}" in 
	*/movies/*)
		TITLE="Movie downloaded"
		cp -r "${TR_DOWNLOADED_PATH}" "${SERENITY_MOVIE_PATH}"
	;;
	*/series/*)
		TITLE="Series downloaded"
		cp -r "${TR_DOWNLOADED_PATH}" "${SERENITY_SERIES_PATH}"
	;;
	*/software/*)
		TITLE="Software downloaded"
		cp -r "${TR_DOWNLOADED_PATH}" "${SERENITY_SOFTWARE_PATH}"
	;;
	*/music/*)
		TITLE="Music downloaded"
		cp -r "${TR_DOWNLOADED_PATH}" "${SERENITY_MUSIC_PATH}"
	;;	
	*)
	    TITLE="Torrent downloaded"
	;;
esac

# Send the Notification
curl -s \
	-F "token=${API_KEY}" \
	-F "user=${USER_KEY}" \
	-F "title=${TITLE}" \
	-F "message=${TR_TORRENT_NAME}" \
	"${URL}" > /dev/null 2>&1